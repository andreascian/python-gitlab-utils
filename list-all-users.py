
import gitlab
import click


def add_members(gl, group, users, members):
    for m in members:
        # print("having member %s" % (m.username))
        if (m.name, m.username) not in users:
            users.append((m.name, m.username))
            print("adding member %s for %s" % (m.username, group.name))
    return users


@click.command()
@click.option('--instance', default='DAVE')
@click.option('--group', "gitlab_group", default=None)
def list_all_users(instance, gitlab_group):
    users = []
    gl = gitlab.Gitlab.from_config(instance)

    group = gl.groups.get(gitlab_group)

    members = group.members.all(all=True)
    add_members(gl, group, users, members)

    for subgroup in group.subgroups.list():
        real_group = gl.groups.get(subgroup.id, lazy=False)
        # print("parsing group %s" % (real_group.name))
        members = real_group.members.all(all=True)
        add_members(gl, real_group, users, members)

    for u in users:
        print(u)


if __name__ == '__main__':
    list_all_users(auto_envvar_prefix='GITLAB')
