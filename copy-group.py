
# copy all project within a group between two gitlab instances
# we assume that both source and destination group already exists
# this copies
# * project name
# * default branch

import gitlab
import click
import git
import tempfile


@click.command()
@click.option('--source-instance', 'source_instance', default='DAVE')
@click.option('--dest-instance', 'dest_instance', default='DAVE')
@click.option('--source-namespace', "source_namespace", default=None)
@click.option('--destination-namespace', 'destination_namespace', default=None)
def fork_group(
                source_instance, dest_instance, source_namespace,
                destination_namespace):

    source_gl = gitlab.Gitlab.from_config(source_instance)
    dest_gl = gitlab.Gitlab.from_config(dest_instance)

    group_to_fork = source_gl.groups.get(source_namespace)
    destination_group = dest_gl.groups.get(destination_namespace)  # noqa

    for group_prj in group_to_fork.projects.list():
        project = source_gl.projects.get(group_prj.id)
        print(
            "copy %s from %s to %s" %
            (project.name, source_namespace, destination_namespace))
        dest_project = dest_gl.projects.create({'name': project.name, 'namespace_id': destination_group.id})

        # get a temporary directory
        dir = tempfile.mkdtemp()
        repo = git.Repo.clone_from(project.ssh_url_to_repo, dir)
        dest_remote = repo.create_remote('dest_remote', dest_project.ssh_url_to_repo)
        dest_remote.push(refspec='{}:{}'.format(project.default_branch, project.default_branch))


if __name__ == '__main__':
    fork_group(auto_envvar_prefix='GITLAB')
