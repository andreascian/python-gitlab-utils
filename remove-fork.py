
import gitlab
from gitlab.exceptions import GitlabHttpError
import click


def remove_fork_from_project(project):
    try:
        project.delete_fork_relation()
        print("project %s fork relation deleted" % (project.name))
    except GitlabHttpError:
        print(
            "something wrong while removing fork from %s "
            "(maybe it does not have any fork?)" % (project.name))


@click.command()
@click.option('--instance', default='DAVE')
@click.option('--namespace', "namespace", default=None)
@click.option('--project-name', 'project_name', default=None)
def remove_fork_relation(instance, namespace, project_name):

    print("Using instance %s" % instance)
    print("Namespace %s, project %s" % (namespace, project_name))

    gl = gitlab.Gitlab.from_config(instance)

    if namespace:
        group = gl.groups.get(namespace)

        for group_prj in group.projects.list():
            project = gl.projects.get(group_prj.id)
            remove_fork_from_project(project)

    elif project_name:
        project = gl.projects.get(project_name)
        remove_fork_from_project(project)


if __name__ == '__main__':
    remove_fork_relation(auto_envvar_prefix='GITLAB')
