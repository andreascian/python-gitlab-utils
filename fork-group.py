
import gitlab
import click


@click.command()
@click.option('--instance', default='DAVE')
@click.option('--source-namespace', "source_namespace", default=None)
@click.option('--destination-namespace', 'destination_namespace', default=None)
def fork_group(instance, source_namespace, destination_namespace):
    gl = gitlab.Gitlab.from_config(instance)

    group_to_fork = gl.groups.get(source_namespace)
    destination_group = gl.groups.get(destination_namespace)  # noqa

    for group_prj in group_to_fork.projects.list():
        project = gl.projects.get(group_prj.id)
        print(
            "forking %s from %s to %s" %
            (project.name, source_namespace, destination_namespace))

        project.forks.create({'namespace': destination_namespace})


if __name__ == '__main__':
    fork_group(auto_envvar_prefix='GITLAB')
