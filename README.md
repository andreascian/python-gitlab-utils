# Abstract

This is my own collection of GitLab utilities based on [python-gitlab](https://github.com/python-gitlab/python-gitlab) API wrapper

This README is just a draft and will be filled later with more information about the project

# Initial setup

It's assumed that you setup a Python 3 environment with [virtualenv](https://virtualenv.pypa.io/en/latest/) as follows

```bash
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

# Utilities usage

All utilities supports the `--help` switch, so use it to have more information about the various options available

## fork-group

This utility fork a whole namespace into another namespace, by iterating on all project of the source namespace

Use it as

```bash
python3 fork-group.py --instance gitlab.com --source-namespace my-source-namespace --destination-namespace your-username/my-destination-group
```

## remove-fork

This remove fork relationship from a give project or for all project of a given namespace

```bash
python3 remove-fork.py --instance gitlab.com --namespace your-username/your-group
```

or

```bash
python3 remove-fork.py --instance gitlab.com --project-name your-username/your-group/your-single-project
```
